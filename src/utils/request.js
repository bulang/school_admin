import axios from 'axios';
import {BaseUrl} from './config';
const service = axios.create({
    headers:{'Content-Type': 'application/json;charset=utf-8'},
    baseURL: BaseUrl,
    timeout: 5000
})

service.interceptors.request.use( config => {
    const token = localStorage.getItem('token')
    if(config.method === 'post') {
        config.data = {
            adminToken:token,
            platform:"ADMIN",
            ...config.data
        }
    } else if(config.method === 'get') {
        config.params = {
            adminToken:token,
            platform:"ADMIN",
            ...config.data
        }
    }
    return config;
}, error => {
    console.log(error);
    return Promise.reject(error);
})

service.interceptors.response.use(response => {
    if(response.status === 200){
        if(response.data.flag==1){
            return Promise.resolve(response.data);
        }else if(response.data.flag==-100){
            localStorage.clear();
            window.location.href="";
        }else{
           return Promise.reject(response.data);
        }
    }else{
       return Promise.reject(response.data);
    }
}, error => {
    console.log(error);
    return Promise.reject(error);
})

export default service;