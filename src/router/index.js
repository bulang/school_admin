import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            redirect: '/dashboard'
        },
        {
            path: '/',
            component: resolve => require(['../components/common/Home.vue'], resolve),
            meta: { title: '自述文件' },
            children:[
                {
                    path: '/dashboard',
                    component: resolve => require(['../components/page/Dashboard.vue'], resolve),
                    meta: { title: '系统首页' }
                },
                {
                    path: '/school',
                    component: resolve => require(['../components/page/school.vue'], resolve),
                    meta: { title: '学校列表' }
                },
                {
                    path: '/category',
                    component: resolve => require(['../components/page/category.vue'], resolve),
                    meta: { title: '分类列表' }
                },
                {
                    path: '/user',
                    component: resolve => require(['../components/page/user.vue'], resolve),
                    meta: { title: '用户列表' }
                },
                {
                    path: '/post',
                    component: resolve => require(['../components/page/post.vue'], resolve),
                    meta: { title: '帖子列表' }
                },
                {
                    path: '/comment',
                    component: resolve => require(['../components/page/comment.vue'], resolve),
                    meta: { title: '评论列表' }
                },
                {
                    path: '/report',
                    component: resolve => require(['../components/page/report.vue'], resolve),
                    meta: { title: '审核列表' }
                },
                {
                    path: '/giftList',
                    component: resolve => require(['../components/page/giftList.vue'], resolve),
                    meta: { title: '礼物列表' }
                },
                {
                    path: '/orderList',
                    component: resolve => require(['../components/page/orderList.vue'], resolve),
                    meta: { title: '订单列表' }
                },
                {
                    path: '/addGift',
                    component: resolve => require(['../components/page/addGift.vue'], resolve),
                    meta: { title: '添加礼物' }
                }
            ]
        },
        {
            path: '/login',
            component: resolve => require(['../components/page/Login.vue'], resolve)
        },
        {
            path: '*',
            redirect: '/404'
        }
    ]
})
