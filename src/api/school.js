import request from '../utils/request';
let getSchoolData={
    Page:1,
    PageSize:10
}
/**
 * 请求学校列表
 * @param {请求类型first/more} options 
 */
export const getSchoolList=(options)=>{
    getSchoolData.Page = options.type=="first"?1:getSchoolData.Page+1;
    return request({
        url:"/gateway/admin/school/list",
        method:"GET",
        data:getSchoolData
    });
}