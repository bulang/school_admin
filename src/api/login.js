import request from '../utils/request';

export const login = (query) => {
    return request({
        url: '/gateway/admin/admin/loginByUsername',
        method: 'post',
        data: query
    })
}