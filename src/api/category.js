import request from '../utils/request';
let getCategoryData={
    Page:1,
    PageSize:10
}
export const getCatogoryList = (options) => {
    getCategoryData.Page = options.type=="first"?1:getCategoryData.Page+1;
    return request({
        url: '/gateway/admin/category/list',
        method: 'GET',
        data: getCategoryData
    })
}