import request from '../utils/request';
let getUserData={
    Page:1,
    PageSize:10,
    nickname:null,
    schoolId:null
}
/**
 * 请求用户列表
 * @param {请求类型first/more} options 
 */
export const getUserList=(options)=>{
    getUserData.Page = options.Page || 1;
    getUserData.nickname = options.nickname || "";
    getUserData.schoolId = options.schoolId || null;
    getUserData.identity = options.Identity || '';
    return request({
        url:"/gateway/admin/user/list",
        method:"GET",
        data:getUserData
    });
}

/**
 * 用户禁言与解禁
 */
export const ban = (options)=>{
    const url = options.type==="ban"?"/gateway/admin/user/ban":"/gateway/admin/user/unban"
    return request({
        url:url,
        method:"POST",
        data:{
            userId:options.userId
        }
    });
}

/**
 * 添加马甲用户
 */
export const addVest = (options)=>{
    return request({
        url:'/gateway/admin/user/addVest',
        method:"POST",
        data:options
    });
}