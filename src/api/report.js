import request from '../utils/request';

/**
 *请求举报列表
 */
export const getReportList = (query) => {
    return request({
        url: '/gateway/admin/report/list',
        method: 'GET',
        data: query
    })
}

/**
 * 举报审核
 */
export const audit = (query)=>{
    return request({
        url:"/gateway/admin/report/audit",
        method:"POST",
        data:query
    });
}

