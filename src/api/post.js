import request from '../utils/request';

/**
 * 请求帖子列表
 * @param {请求类型first/more} options 
 */
export const getPostList=(options)=>{
    return request({
        url:"/gateway/admin/post/list",
        method:"GET",
        data: {...options}
    });
}

/**
 * 帖子上下架
 */
export const subPost = (options)=>{
    const url = options.type == 'on'?'/gateway/admin/post/display':'/gateway/admin/post/hide';
    return request({
        url:url,
        method:"POST",
        data:{...options}
    });
}

/**
 * 马甲号发布帖子
 */
export const publishPost = (options)=>{
    return request({
        url:"/gateway/admin/post/create",
        method:"POST",
        data:{...options}
    });
}