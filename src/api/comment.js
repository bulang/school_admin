import request from '../utils/request';

export const getCommentList = (query) => {
    return request({
        url: '/gateway/admin/comment/list',
        method: 'GET',
        data: query
    })
}

