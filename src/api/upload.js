import axios from 'axios';
import {BaseUrl} from '../utils/config';
export const uploadFile = (files)=>{
    let myuploadTask = uploadTasks(files);
    return new Promise((resolve, reject) => {
        Promise.all(myuploadTask)
            .then(res => {
                resolve(res);
            })
            .catch(err => {
                reject(err);
            });
    });
}

const uploadTasks = (files)=>{
    let uploadTasks = files.map(file=>{
        return new Promise((resolve,reject)=>{
             let objform = new FormData();
             objform.append('file',file);
             let config = {
                 headers: {
                 'Content-Type': 'multipart/form-data;charset=utf-8'
                 }
             }
             axios.post(BaseUrl+'/gateway/api/system/upload', objform, config).then(function (response) {
                 if(response.status==200){
                     resolve(response.data);
                 }
             })
         });
     });
     return uploadTasks;
}