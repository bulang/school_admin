import request from '../utils/request';

/**
 * 请求礼物列表
 */
export const getGiftList = (query)=>{
    return request({
        url:"/gateway/admin/gift/list",
        method:"GET",
        data:query
    });
}

/**
 * 获取礼物详情
 */
export const getGift = (query)=>{
    return request({
        url:"/gateway/admin/gift/get",
        method:"GET",
        data:query
    });
}

/**
 * 添加礼物
 */
export const addGift = (query)=>{
    return request({
        url:"/gateway/admin/gift/add",
        method:"POST",
        data:query
    });
}

/**
 * 上下架礼物
 */
export const subGift = (options)=>{
    const url = options.type=="on"?"/gateway/admin/gift/on":"/gateway/admin/gift/off";
    return request({
        url: url,
        method:"POST",
        data:{
            giftId:options.giftId
        }
    });
}

/**
 * 订单列表
 */

export const getOrderList = (query)=>{
    return request({
        url:"/gateway/admin/gift/orders",
        method:"GET",
        data:query
    });
}

 /**
  * 发货
  */
 export const ship = (options)=>{
    return request({
        url:"/gateway/admin/gift/ship",
        method:"POST",
        data:options
    })
 }